/**
 * 全局中间变量，用于接收页面数据,最后交于vue，防止多个组件重复定义变量
 */
let pageData = {} // 页面数据
let pageMethods = {} // 页面方法