window.addEventListener('load',()=>{
  const { ElMessage } = ElementPlus

  // vue加载
  let insertData = {}
  let insertMethods = {}
  // 接收页面script中定义的data
  if(typeof(pageData) !== "undefined") insertData = pageData
  // 接收页面script中定义的methods
  if(typeof(pageMethods) !== "undefined") insertMethods = pageMethods

  // 通用方法
  let methods = {
    back(href){
      // 返回首页
      if(href == 'index') return location.href = '/'
      // 返回上一级页面
      history.back()
    },
    timestampFormat(timestamp){
      return  timestampFormat(timestamp)
    },
    ...insertMethods,
  }

  let App = {data(){return { ...insertData, message:ElMessage ,drawer: false} }, methods}
  const app = Vue.createApp(App)

  app.use(ElementPlus)
  app.mount("#app")

  // 睡眠函数
  function sleep(delay) {
    let start = (new Date()).getTime();
    while ((new Date()).getTime() - start < delay) {
      continue;
    }
  }
  // 延迟1s关闭laoder
  sleep(1000)
  // loader关闭
  const loader = document.querySelector('.common-loader');
  loader.style.display = 'none';


})